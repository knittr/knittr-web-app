import {
  FETCH_ARTICLE,
  SEARCH_ARTICLES,
  CLEAR_SEARCH_ARTICLES,
  LOAD_DUMMY_SEARCH,
  REMOVE_ARTICLE,
  HANDLE_ERROR
} from './actionTypes';

import axios from 'axios';

//fetches a single article that was selected by the user
export function fetchArticle(id) {
  return (dispatch) => {
    dispatch({
      type: REMOVE_ARTICLE
    });
    axios.post("/app/article", {
      id
    })
    .then(resp => {
      //TODO: check if backend sends invalid response or throws error
      let article = resp.data;
      if(resp.data.invalid) {
        dispatch({
          type: HANDLE_ERROR,
          errorType: "selectedArticle"
        });
        return ;
      }
      dispatch({
        type: FETCH_ARTICLE,
        article
      });
    })
    .catch(err => {
      dispatch({
        type: HANDLE_ERROR,
        errorType: "selectedArticle"
      });
    });
  }
}

//fetches the articles searched by user
export function searchArticles(name) {
  return (dispatch) => {
    dispatch({
      type: LOAD_DUMMY_SEARCH
    });
    axios.post("/app/search", {
      title: name
    })
    .then(resp => {
      //TODO: check if backend sends invalid response or throws error
      let searchedArticles = resp.data;
      dispatch({
        type: SEARCH_ARTICLES,
        searchedArticles
      });
      if(resp.data.invalid) {
        dispatch({
          type: HANDLE_ERROR,
          errorType: "searchResults"
        });
      }
    })
    .catch(err => {
      dispatch({
        type: HANDLE_ERROR,
        errorType: "searchResults"
      });
    });
  }
}

//clears searched articles
export function clearSearchArticles() {
  return {
    type: CLEAR_SEARCH_ARTICLES
  }
}

//clears the fetched article when user navigates to different page
export function removeArticle() {
  return {
    type: REMOVE_ARTICLE
  }
}