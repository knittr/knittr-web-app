//authentication action creators
export { signup } from './authAction';

//bookmarks action creators
export {
  fetchBookmarks,
  addBookmark,
  removeBookmark,
  removeBookmarks
} from './bookmarksAction';

//articles action creators
export {
  fetchArticle,
  searchArticles,
  clearSearchArticles,
  removeArticle
} from './articlesAction';

//feeds action creators
export {
  fetchFeeds,
  fetchAdditionalFeeds
} from './feedsAction';

export { uploadFiles } from './customAnalysisAction';