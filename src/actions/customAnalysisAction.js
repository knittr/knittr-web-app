import {UPLOAD_FILES, LOADING, HANDLE_ERROR} from './actionTypes';
import axios from 'axios';

export function uploadFiles(data) {
  console.log(data)
  return (dispatch) => {
    dispatch({
      type: LOADING,
      status: true
    });
    axios.post('http://127.0.0.1:5000/analysis', data)
    .then((res) => {
      console.log(res.data)
      dispatch({
        type: UPLOAD_FILES,
        data: res.data
      });
      dispatch({
        type: LOADING,
        status: false
      });
    })
    .catch(function (error) {
      dispatch({
        type: LOADING,
        status: false
      });
      dispatch({
        type: HANDLE_ERROR,
        errorType: 'CUSTOM_ERROR'
      })
    });
  }
}