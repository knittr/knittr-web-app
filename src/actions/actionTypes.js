/********** ERROR HANDLING ACTION START *********/
//TODO: FIX ALL THE ERROR HANDLING
export const HANDLE_ERROR = 'HANDLE_ERROR';
/********** ERROR HANDLING ACTION END *********/

/********** AUTHENTICATION ACTIONS START *********/
//login and logout endpoints
export const SIGN_UP = 'SIGN_UP';

//store action
export const CHECK_AUTHENTICATION = 'CHECK_AUTHENTICATION';
/********** AUTHENTICATION ACTIONS END *********/

/********** BOOKMARKS ACTION START *********/
//data endpoints and store actions
export const FETCH_BOOKMARKS = 'FETCH_BOOKMARKS';
export const ADD_BOOKMARK = 'ADD_BOOKMARK';
export const REMOVE_BOOKMARK = 'REMOVE_BOOKMARK';
export const REMOVE_BOOKMARKS = 'REMOVE_BOOKMARKS';
/********** BOOKMARKS ACTIONS END *********/

/********** FEEDS ACTIONS START *********/
//data endpoints and store actions
export const FETCH_FEEDS = 'FETCH_FEEDS';
export const FETCH_ADDITIONAL_FEEDS = 'FETCH_ADDITIONAL_FEEDS';
/********** FEEDS ACTIONS END *********/

/********** ARTICLE ACTIONS START *********/
//data endpoints and store actions
export const FETCH_ARTICLE = 'FETCH_ARTICLE';
export const SEARCH_ARTICLES = 'SEARCH_ARTICLES';
export const LOAD_DUMMY_SEARCH = 'LOAD_DUMMY_SEARCH';

//store action
export const REMOVE_ARTICLE = 'REMOVE_ARTICLE';
export const CLEAR_SEARCH_ARTICLES = 'CLEAR_SEARCH_ARTICLES';
/********** ARTICLE ACTIONS END *********/

/********** CUSTOM ANALYSIS ACTIONS START *********/
export const UPLOAD_FILES  = 'UPLOAD_FILES';
export const LOADING = 'LOADING';