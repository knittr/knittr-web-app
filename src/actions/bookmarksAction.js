import {
  FETCH_BOOKMARKS,
  ADD_BOOKMARK,
  REMOVE_BOOKMARK,
  REMOVE_BOOKMARKS
} from './actionTypes';

import axios from 'axios';

//TODO: fix the urls
//fetches bookmarks on initial load.
export function fetchBookmarks() {
  return (dispatch) => {
    //TODO: fix call type
    axios.post("http://localhost:3000/bookmarks")
      .then(resp => {
        dispatch({
          type: FETCH_BOOKMARKS,
          bookmarks: resp.data
        });
      })
      .catch(err => {
        dispatch({
          type: HANDLE_ERROR,
          errorType: "loadingBookmarks"
        });
      });
  }
}

//adds a bookmark
export function addBookmark(id) {
  return (dispatch) => {
    //TODO: fix call type
    //response should be the bookmarked article
    axios.post("")
      .then(resp => {
        let { bookmark } = resp.data
        dispatch({
          type: ADD_BOOKMARK,
          bookmark
        });
      })
      .catch(err => {
        dispatch({
          type: HANDLE_ERROR,
          errorType: "addingBookmark"
        });
      });
  }
}

//removes a bookmark
export function removeBookmark(id) {
  return (dispatch) => {
    //TODO: fix call type
    //response should be the ID of the bookmarked article
    axios.post("")
      .then(resp => {
        dispatch({
          type: REMOVE_BOOKMARK,
          id
        });
      })
      .catch(err => {
        dispatch({
          type: HANDLE_ERROR,
          errorType: "removingBookmark"
        });
      });
  }
}

//removes all the bookmarks
export function removeBookmarks(id) {
  return (dispatch) => {
    //TODO: fix call type
    //response should be true or false
    axios.post("")
      .then(resp => {
        dispatch({
          type: REMOVE_BOOKMARKS,
          bookmarks: []
        });
      })
      .catch(err => {
        dispatch({
          type: HANDLE_ERROR,
          errorType: "removingBookmarks"
        });
      });
  }
}
