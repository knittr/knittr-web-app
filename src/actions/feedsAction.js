import {
  FETCH_FEEDS,
  FETCH_ADDITIONAL_FEEDS,
  HANDLE_ERROR
} from './actionTypes';

import axios from 'axios';

//helper function that fetches initial and on demand feeds
function fetcher(actionType, dispatch) {
  axios.post("/app/feed")
    .then(resp => {
      let feeds = resp.data;
      //TODO: check if backend sends invalid response or throws error
      if(resp.data.invalid) {
        dispatch({
          type: HANDLE_ERROR,
          errorType: "loadingFeeds"
        });
        return;
      }
      dispatch({
        type: actionType,
        feeds
      });
    })
    .catch(err => {
      dispatch({
        type: HANDLE_ERROR,
        errorType: "loadingFeeds"
      });
    });
}

//fetches initial feeds
export function fetchFeeds() {
  return (dispatch) => {
    fetcher(FETCH_FEEDS, dispatch);
  }
}

//fetches additional feeds
export function fetchAdditionalFeeds() {
  return (dispatch) => {
    fetcher(FETCH_ADDITIONAL_FEEDS, dispatch);
  }
}