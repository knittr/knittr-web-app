import { HANDLE_ERROR } from '../actions/actionTypes';
import { LOADING } from '../actions/actionTypes';

//handles all the error types
export default function errorTypeReducer(state = "", action) {
  let { type, errorType } = action;
  switch(type) {
    case HANDLE_ERROR:
      return errorType
      break;
    default:
      return state;
  }
}

function loadingReducer(state= false, action) {
  let {type, status} = action;
  switch(type) {
    case LOADING:
      return status
      break;
    default:
      return state;
  }
}

//single article data for animation
const dummyArticle = {
  id: null,
  thumbnail: null,
  headline: null,
  subject: null,
  rating: null,
  tags: [null, null, null, null, null, null, null, null, null, null, null, null],
  links: [],
  content: null
};

//feeds data for animation
export let dummyFeeds = [], i = 0;
let dum = 0;
while(dum < 6) {
  dummyArticle.links.push({ link: null, source: null, title: null });
  dum++;
}

while(i < 13) {
  dummyFeeds.push({
    id: null,
    thumbnail: null,
    headline: null,
    location: null,
    content: null
  });
  i++;
}
//bookmarks and search data for animation
let dummyBookmarks = [], dummyResults = [], j = 0;

while(j < 8) {
  dummyBookmarks.push({
    id: null,
    thumbnail: null,
    headline: null,
    location: null
  });
  j++;
  if(j == 7) {
    dummyResults = dummyBookmarks;
  }
}

export {dummyArticle, loadingReducer, dummyResults, dummyBookmarks};