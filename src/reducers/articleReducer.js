import { 
  FETCH_ARTICLE,
  SEARCH_ARTICLES,
  CLEAR_SEARCH_ARTICLES,
  LOAD_DUMMY_SEARCH,
  REMOVE_ARTICLE
} from '../actions/actionTypes';
import { dummyArticle, dummyResults } from './util';

//a single article that was selected
export function articleReducer(state = {...dummyArticle}, action) {
  let { type, article } = action;
  switch(type) {
    case FETCH_ARTICLE:
      return { ...article };
      break;
    case REMOVE_ARTICLE:
      return { ...dummyArticle };
      break;
    default:
      return state;
  }
}
//articles from search results
export function searchedArticlesReducer(state = [], action) {
  let { type, searchedArticles } = action;
  switch(type) {
    case LOAD_DUMMY_SEARCH:
      return [...dummyResults];
      break;
    case SEARCH_ARTICLES:
      return [...searchedArticles];
      break;
    case CLEAR_SEARCH_ARTICLES:
      return [];
      break;
    default:
      return state;
  }
}