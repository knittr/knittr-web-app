import { SIGN_UP } from '../actions/actionTypes';

export default function signupReducer(state= "", action) {
  switch(action.type) {
    case SIGN_UP:
      return action.userID;
      break;
    default:
      return state;
  }
}