import { combineReducers } from 'redux';
import signupReducer from './authReducer';
import bookmarksReducer from "./bookmarksReducer";
import {feedsReducer} from './feedsReducer';
import {articleReducer, searchedArticlesReducer} from './articleReducer';
import { customAnalysisReducer } from './customAnalysisReducer';
import errorTypeReducer, {loadingReducer} from './util';

let rootReducer = combineReducers({
  bookmarks: bookmarksReducer,
  feeds: feedsReducer,
  article: articleReducer,
  searchedArticles: searchedArticlesReducer,
  userID: signupReducer,
  errorType: errorTypeReducer,
  customData: customAnalysisReducer,
  loadingStatus: loadingReducer
});

export default rootReducer;