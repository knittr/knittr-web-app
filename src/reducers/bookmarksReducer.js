import {
  FETCH_BOOKMARKS,
  ADD_BOOKMARK,
  REMOVE_BOOKMARK,
  REMOVE_BOOKMARKS
} from '../actions/actionTypes';
import { dummyBookmarks } from './util';

//handles all bookmarks actions
export default function bookmarksReducer(state = [...dummyBookmarks], action) {
  switch(action.type) {
    case FETCH_BOOKMARKS :
      return [ ...action.bookmarks ];
      break;
    case ADD_BOOKMARK:
      return [
        ...state,
        action.bookmark
      ];
      break;
    case REMOVE_BOOKMARK:
      let newState = state.filter((el) => action.id !== el.id );
      return [
        ...newState
      ];
      break;
    case REMOVE_BOOKMARKS:
      return [ ...action.bookmarks ];
      break;
    default:
    return state;
  }
}