import { FETCH_FEEDS, FETCH_ADDITIONAL_FEEDS } from '../actions/actionTypes';
import { dummyFeeds } from './util';

//handles all feeds actions
export function feedsReducer(state = [...dummyFeeds], action) {
  let { type, feeds } = action;
  switch(type) {
    case FETCH_FEEDS:
      return [ ...feeds ];
      break;
    case FETCH_ADDITIONAL_FEEDS:
      return [ ...state, ...feeds];
      break;
    default:
      return state;
  }
}

