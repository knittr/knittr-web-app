import { UPLOAD_FILES, HANDLE_ERROR } from '../actions/actionTypes'

export function customAnalysisReducer(state = {}, action) {
  let { type, data } = action;
  switch(type) {
    case UPLOAD_FILES:
      let temp = [];
      for(let key in data.sentiment_sentence) {
        if(data.sentiment_sentence.hasOwnProperty(key)) {
          temp.push([key,data.sentiment_sentence[key]])
        }
      }
      return { ...data, sentiment_sentence: temp };
      break;
    default:
      return state;
  }
}