import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { SkeletonTheme } from "react-loading-skeleton";

import { fetchArticle } from '../actions/index';
import ArticleThumbnail from '../components/article/ArticleThumbnail';
import ContentCard from '../components/article/ContentCard';
import Connections from '../components/article/Connections';
import Articlenav from '../components/navbar/Articlenav';

class Article extends Component {
  constructor(props) {
    super(props);
    this.state = {
      viewConnections: false
    }
  }

  toggleConnections = () => {
    document.querySelector(".article__container")
    .style.filter = this.state.viewConnections ? "blur(0)" : "blur(5px)";
    this.setState(prevState => {
      return {
        viewConnections: !prevState.viewConnections
      }
    })
  }

  componentDidMount = () => {
    let id = this.props.match.params.id;
    if(!Number.isInteger(parseInt(id))) {
      this.props.history.replace("/app/notfound");
    }
    this.props.fetchArticle(id);
  }

  render() {
    let { headline, thumbnail, tags, content, links } = this.props.article;
    let { viewConnections } = this.state;
    return (
      <div className="article">
        <div className="article__container">
          <SkeletonTheme color="#6d6d6d" highlightColor="#202020">
            <ArticleThumbnail {...{headline, thumbnail, tags}} {...this.props}/>
            <div className="article__container-cards">
              <ContentCard content={content} />
            </div>
          </SkeletonTheme>
        </div>
        <Articlenav toggleConnections={this.toggleConnections} />
        {viewConnections ? <Connections
          links={links}
          viewConnections={viewConnections}
          toggleConnections={this.toggleConnections}
          /> : null}
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    article: state.article
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    fetchArticle
  }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Article);