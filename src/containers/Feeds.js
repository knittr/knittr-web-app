import React, { Component } from 'react';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import { SkeletonTheme } from "react-loading-skeleton";
import uuidv4 from 'uuid/v4';

import {fetchFeeds} from '../actions/index';
import Bottomnav from '../components/navbar/Bottomnav';
import Feed from '../components/article/Feed';
import Topnav from '../components/navbar/Topnav';

class Feeds extends Component {

  constructor(props) {
    super(props);
    this.state = {
      width: window.innerWidth
    }
  }

  componentDidMount = () => {
    this.props.fetchFeeds();
    window.addEventListener("resize", this.updateWidth);
  }

  updateWidth = () => {
    this.setState({
      width: window.innerWidth
    });
  }

  componentWillUnmount = () => {
    window.removeEventListener("resize", this.updateWidth);
  }


  //Generate rows based on screen size with random card being of double size in a row
  contentGen = (feeds, width) => {
    let feedsCopy = Array.from(feeds, x => x);
    let l = feedsCopy.length;
    let tempFeeds = [];
    let rand = 1;
    if(width > 1099) {
      rand = Math.floor(Math.random() * 3);
      for(let i = 0; i < Math.floor(l / 3); i++) {
        tempFeeds.push(feedsCopy.splice(0,3));
      }
      tempFeeds.push(feedsCopy);
    } else if(width < 1100 && width > 700) {
      rand = Math.floor(Math.random() * 2);
      for(let i = 0; i < Math.floor(l / 2); i++) {
        tempFeeds.push(feedsCopy.splice(0,2));
      }
      tempFeeds.push(feedsCopy);
    } else {
      for(let i = 0; i < l; i++) {
        tempFeeds.push(feedsCopy.splice(0,1));
      }
    }

    return tempFeeds.map((item, i) => {
      let tempL = tempFeeds.length;
        if(width > 1099) {
          if(i == tempL - 1 && tempFeeds[tempL - 1].length < 3) {
            return <div key={uuidv4()} className="row">
             { tempFeeds[tempFeeds.length - 1].map((item, i) => {
                return <div key={uuidv4()} className="col-xs-3">
                  <Feed item={item} key={uuidv4()} />
                </div>
              })}
            </div>
          } else {
            rand = Math.floor(Math.random() * 3);
            return <div className="row" key={uuidv4()}>
              {item.map((el, i) => {
                if(i == rand) {
                  return <div className="col-xs-6" key={uuidv4()}>
                  <Feed item={el} />
                </div>
                } else {
                  return  <div className="col-xs-3" key={uuidv4()}>
                  <Feed item={el} />
                </div>
                }
              })}
            </div>
          }
        } else if( width < 1100 && width > 700) {
          rand = Math.floor(Math.random() * 2);
          return <div key={uuidv4()} className="row">
            { item.map((el, i) => {
              if(i == rand) {
                return <div className="col-xs-7" key={uuidv4()}>
                <Feed item={el} />
              </div>
              } else {
                return  <div className="col-xs-5" key={uuidv4()}>
                <Feed item={el} />
              </div>
              }
            })}
          </div>
        } else {
          return <div className="row" key={uuidv4()}>
            <Feed item={item[0]} />
          </div>
        }
    })
  }


  render() {
    return (
      <SkeletonTheme color="#6d6d6d" highlightColor="#202020">
        <div className="feeds">
          <div className="feeds__container">
            <Topnav/>
            <section>
              {this.contentGen(this.props.feeds, this.state.width)}
            </section>
          </div>
          <div className="feeds__bottomnav">
            <Bottomnav />
          </div>
        </div>
      </SkeletonTheme>
    );
  }
}

function mapStateToProps(state) {
  return {
    feeds: state.feeds
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    fetchFeeds
  }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Feeds);