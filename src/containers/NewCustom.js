import React, { Component, Fragment } from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {uploadFiles} from '../actions/index';
import backImg from '../assets/imgs/analysis.jpg'
import TwoLevelPieChart from '../components/custom/TwoLevelPieChart';
import WordsCloud from '../components/custom/WordsCloud';
import loadingImg from '../assets/imgs/puff.svg';


class NewCustom extends Component {
  state ={
    textbox: "",
    fileNames: [],
    toggle: false,
    doc: []
  }

  handleTextChange = (e) => {
    this.setState({
      textbox: e.target.value
    });
  }

  handleUpload = () => {
    let poo = document.getElementById("fileupload");
    let e = document.getElementById("dsl");
    let val = e.options[e.selectedIndex].value;
    let formData = new FormData();

    if(this.state.textbox.length > 0) {
      formData.append('textbox',this.state.textbox);
    } else if(poo.files.length >= 1) {
      let fileNames = [];
      for(let i = 0; i < poo.files.length; i++ ){
        formData.append('file'+i,poo.files[i]);
        fileNames.push(poo.files[i].name);
      }
      this.setState({
        fileNames
      });
    }
    formData.append("sumlevel", val);
    this.props.uploadFiles(formData);
  }

  handleReset = () => {
    this.setState({
      textbox: '',
      fileNames: []
    });
  }

  handlePopup = (data) => {
    console.log(data)
    this.setState({
      doc: data[1] || []
    })
    setTimeout(() => {
      this.setState(prev => {
        return {
          toggle: !prev.toggle
        }
      })
    }, 100);
  }

  render() {
    let { textbox, fileNames, doc } = this.state;
    let { summary,
          no_document,
          no_words,
          diversity,
          keywords,
          wordcloud,
          sentiment_summary,
          sentiment_document,
          sentiment_sentence } = this.props.customData;
    return (
      <div className="custom"
        style={{backgroundImage :
          `linear-gradient(to left top,rgba(0, 0, 0, 0.7), rgba(250, 250, 250, 0.4)), url(${backImg})`
        }}>
        <div className="custom__container">
          <h2>Analysis</h2>
          <div className="section1">
            <section>
              <input
                disabled={textbox.length > 0 ? true : false}
                type='file'
                id="fileupload"
                multiple/>
              <label
              style={textbox.length > 0 ? {backgroundColor: 'rgb(253, 81, 81)'} : {backgroundColor: 'rgb(70, 214, 250)'}}
              className="fileupload" htmlFor="fileupload">
                <i className="fas fa-upload"/>
                Browse Files
              </label>
              <label id="textlabel">Text</label>
              <textarea
                htmlFor="textlabel"
                onChange={this.handleTextChange}
                placeholder="Enter your text here..." 
                id="textupload"
                value={this.state.textbox}
                />
              <label htmlFor="dsl" id="dsllabel">Summarization Level</label>
              <select id="dsl">
                <option value="20">Low</option>
                <option value="15">Medium</option>
                <option value="10">High</option>
                <option value="5">Extreme</option>
              </select>
              <div className="custombtns">
                <span onClick={this.handleUpload} id="upload">UPLOAD</span>
                <span onClick={this.handleReset} id="reset">RESET</span>
              </div>
            </section>
          </div>
          {
            summary ? <div className="section2">
            <div className="docsandwords">
              <div className="docs">
                <h4>Number of Documents</h4>
                <h4>{no_document ? no_document : '-'}</h4>
              </div>
              <div className="words">
                <h4>Total Number of Words</h4>
                <h4>{no_words ? no_words : '-'}</h4>
              </div>
            </div>
            <div className="summary">
              <h3>Summarization</h3>
              <p>
              {
                summary ? typeof(summary) == "string" ? summary : summary.map((item, i) => {
                  let sup = 0;
                  fileNames.map((it, j) => item.name == it ? sup = j : sup = 0)
                  return <Fragment key={i}>{item.sentence}<sup>#{sup + 1}</sup> </Fragment>
                }) : null
              }
              </p>
            </div>
            <div className="diversity">
              <h3>Diversity analysis</h3>
              {
                summary ? diversity ? <div className="filesinput">
                <ol type="1">
                  {
                    diversity.map((item, i) => <li key={i}>
                    <span className="filename">{i+1}. {item.name}</span>
                    <span className="score">{parseInt(parseFloat(item.score) * 100)}%</span>
                  </li>)
                  }
                </ol>
              </div> : <div className="textinput">
                <h4>Text Input</h4>
                <h4>100%</h4>
              </div> : null
              }
            </div>
            <div className="docsentiments">
              <h3>Sentiment analysis</h3>
              <div className="fileslist">
                {
                  sentiment_sentence ? sentiment_sentence.map((item, i) => {
                    let posval = parseInt(sentiment_document[i].pos * 100);
                    let neuval = parseInt(sentiment_document[i].neu * 100);
                    let negval = parseInt(sentiment_document[i].neg * 100);
                    return <div key={i} className="file">
                    <div className="filename">
                      <span className="name">{i+1}. {item[0]}</span>
                      <span onClick={() => this.handlePopup(item)} className="btn">expand</span>
                    </div>
                    <div className="bar">
                      <span className="green" style={{flex: `0 0 ${posval}`}}>{posval}%</span>
                      <span className="yellow" style={{flex: `0 0 ${neuval}`}}>{neuval}%</span>
                      <span className="red" style={{flex: `0 0 ${negval}`}}>{negval}%</span>
                    </div>
                  </div>
                  }) : null
                }
              </div>
            </div>
            <div className="piechart">
              <h3>Overall sentiment analysis</h3>
              {
                summary ? <TwoLevelPieChart data={sentiment_summary} /> : null
              }
            </div>
            <div className="keywords">
              <h3>Keywords</h3>
              <ol>
                {summary ? keywords.map((item, i) => <li key={i}>{item}</li>) : null}
              </ol>
            </div>
            <div className="wordcloud">
              <h3>Wordcloud</h3>
              {
                summary ? <WordsCloud data={wordcloud} /> : null
              }
            </div>
          </div> : null
          }
        </div>
        { this.state.toggle || this.props.loading ? this.props.loading ? <div className="loading">
            <img src={loadingImg} />
          </div> : <div className="singledoc">
          <div className="back"></div>
          <div className="content">
            <div>
            <p>
              {
                doc.map((item,i) => {
                  return <span key={i} style={{backgroundColor: item.value == "pos" ? "green" : item.value == "neg" ? "red" : "yellow"}}>{item.name}</span>
                })
              }
            </p>
            <span onClick={this.handlePopup} className="close">CLOSE</span>
            </div>
          </div>
        </div> : null}
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    customData: state.customData,
    loading: state.loadingStatus
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    uploadFiles
  }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(NewCustom);