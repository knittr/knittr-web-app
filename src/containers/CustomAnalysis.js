import React, {Component} from "react";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { uploadFiles } from '../actions/index';
import puff from '../assets/imgs/puff.svg';
import TwoLevelPieChart from '../components/custom/TwoLevelPieChart';
import WordsCloud from '../components/custom/WordsCloud';

class CustomAnalysis extends Component {
  state ={
    textbox: ""
  }

  handleTextChange = (e) => {
    this.setState({
      textbox: e.target.value
    });
  }

  handleUpload = () => {
    var poo = document.getElementById("fileupload");
    if(this.state.textbox.length > 0) {
      this.props.uploadFiles({
        textbox: this.state.textbox
      });
    } else if(poo.files.length > 1) {
      var formData = new FormData();
      for(let i = 0; i < poo.files.length; i++ ){
        formData.append('file'+i,poo.files[i]);
      }
      this.props.uploadFiles(formData);
    }
  }

  render() {
    let { keywords, sentiment, summary, wordcloud } = this.props.customData;
    console.log(sentiment)
    return (
      <div className="customanalysis">
        <div className="containertop">
          <div className="containertop__para">
            <p>Upload your documents and get their summarization, word cloud and T-sne graph.</p>
          </div>
          <div className="containertop__upload">
            <input
              disabled={this.state.textbox.length > 0 ? true : false}
              type='file'
              id="fileupload"
              onChange={this.handleChange}
              multiple/>
            <label htmlFor="fileupload"><i className="fas fa-upload"></i>Browser Files</label>
          </div>
        </div>
        <textarea
          onChange={this.handleTextChange}
          placeholder="Enter your text here..." 
          id="textupload"
          value={this.state.textbox}
        >
        </textarea>
        <button id="uploadbtn" onClick={this.handleUpload}>Upload</button>
        {
          summary && summary.length > 10 ? <div className="containerbottom">
          <div className="containerbottom__summary">
            <div>
              <p>{summary ? summary : null}</p>
            </div>
            <div>
              <ul>
                {keywords.map((item, i) => <li key={i}>{item}</li>)}
              </ul>
            </div>
          </div>
          <div className="containerbottom__charts">
            <WordsCloud data={wordcloud}/>
            <TwoLevelPieChart data={sentiment}/>
          </div>
        </div> : null
        }
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    customData: state.customData
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    uploadFiles
  }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(CustomAnalysis);
