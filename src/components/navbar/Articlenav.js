import React, { Component } from 'react';

class Articlenav extends Component {
  constructor(props) {
    super(props);
    this.state = {
      width: window.innerWidth
    }
  }

  componentDidMount = () => {
    window.addEventListener("resize", this.updateWidth);
  }

  updateWidth = () => {
    this.setState({
      width: window.innerWidth
    });
  }

  componentWillUnmount = () => {
    window.removeEventListener("resize", this.updateWidth);
  }
  
  render() {
    let { width } = this.state;
    let temp = width < 600;
    return (
      <div className="articlenav">
        <p>
          <i className="fas fa-font"></i>{temp ? null : "Settings"}
        </p>
        <p onClick={this.props.toggleConnections}>
          <i className="fas fa-link"></i>{temp ? null : "Connections"}
        </p>
        <p>
          <i className="far fa-bookmark"></i>{temp ? null : "Bookmark"}
        </p>
      </div>
    );
  }
}

export default Articlenav;