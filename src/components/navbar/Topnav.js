import React, { Component } from 'react';

class Topnav extends Component {
  render() {
    return (
      <div className="topnav">
        <h2 className="topnav__trending">Trending</h2>
        <h2 className="topnav__bookmarks">Bookmarks</h2>
      </div>
    );
  }
}

export default Topnav;