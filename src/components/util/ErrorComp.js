import React from 'react'
import { Link } from 'react-router-dom';

const PageNotFound = () => {
  return (
    <div className="pagenotfound">
      <div>
        <h2>Are you lost?</h2>
        <h3>Come i'll take you home.</h3>
        <Link to="/"><button className="btn">Go to home</button></Link>
      </div>
    </div>
  )
}

const Refresh = () => {
  return (
    <div>
      <h3>Something went wrong!</h3>
      <span ><i class="fas fa-redo"></i></span>
    </div>
  )
}

export { PageNotFound, Refresh };