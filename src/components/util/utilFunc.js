import $ from "jquery";

//extracts content out of the article body by removing "center" tag content
function extractContent(s) {
  console.log("object")
  return s.replace(/(&nbsp;|<([^>]+)>)/ig, "");
}

function setFontSize(val) {
  let value = val || localStorage.getItem("knFontSize");
  let h2Font = parseInt(value) + 10, h3Font = parseInt(value) + 5;
  $(".article__container h2").css("font-size", `${h2Font}px`);
  $(".article__container h3").css("font-size", `${h3Font}px`);
  let paras = Array.from(document.querySelectorAll(".article__container p"));
  paras[0] && paras.map(el => { 
    el.style.fontSize = parseInt(value) + "px";
    el.style.lineHeight = (parseInt(value) + 2) + "px";
  });
}

function setFontFamily(val) {
  let font = val || localStorage.getItem("knFontFamily");
  let paras = Array.from(document.querySelectorAll(".article__container p"));
  paras[0] && paras.map(el => { el.style.fontFamily = `${font}, 'Times New Roman', serif`;});
}


export { extractContent, setFontSize, setFontFamily };