import React from 'react';
import { Link } from 'react-router-dom';
import Skeleton from 'react-loading-skeleton';
import { extractContent } from './utilFunc';

//creates a single article of news feeds, bookmarks, search results.
const Article = (props) => {
  let { id, thumbnail, headline, location, rating, content } = props.item;
  return(
    <div className="feed" onClick={props.handleClose}>
      <div className="thumbnail">
        <Link to={`/article/${id ? id : ""}`}>
          { thumbnail ? <img src={thumbnail} alt={headline} /> : <Skeleton /> }
        </Link>
      </div>
      <div className="content">
        <div className="headline_rating">
          <div className="headline">
            <Link to={`/article/${id ? id : ""}`}>
              <h2>{headline ? headline : <Skeleton/>}</h2>
            </Link>
            <h3>{location ? location : <Skeleton/>}</h3>
          </div>
          <div className="rating">
            <p>{rating ? rating : <Skeleton />}</p>
            <span>{ rating ? <i className="far fa-bookmark"></i> : <Skeleton/>}</span>
          </div>
        </div>
        <div className="initial_content">
          { content ? extractContent(content) : <Skeleton count={4} />}
        </div>
      </div>
    </div>
  )
}

const Bookmark = (props) => {
  let { thumbnail, headline, location } = props.item;
  return (
    <div className="bookmark">
      <div className="thumbnail">
        { thumbnail ? <img src={thumbnail} alt={headline} /> : <Skeleton />}
      </div>
      <div className="title">
        <h2>
          {headline ? headline : <Skeleton count={2} />}
        </h2>
        <h3>{location ? location : <Skeleton />}</h3>
      </div>
    </div>
  )
}

//navbar to be shown when connections are opened
const ConOpenNav = (props) => {
  if(props.width < 500) {
    return (
      <div className="share-smbtns">
        <span><i className="far fa-bookmark"></i></span>
        <span><i className="fas fa-share-alt"></i></span>
        <span><i className="fab fa-facebook-f"></i></span>
        <span><i className="fab fa-google-plus-g"></i></span>
      </div>
    )
  } else {
    return (
      <div className="share-txtbtns">
        <span><i className="far fa-bookmark"></i>Bookmark</span>
        <span><i className="fas fa-share-alt"></i>Share</span>
        <span><i className="fab fa-facebook-f"></i>Share via Facebook</span>
        <span><i className="fab fa-google-plus-g"></i>Share via Google</span>
      </div>
    )
  }
}

//connections container of bottom navbar
const ConContainer = (props) => {
  let {  links, handleShow } = props;
  return (
    <div  className="connections__container">
      <div className="connections">
        <ul>
          {links[0] !== null && links.map((el, i) => {
            return <span key={i}>
                <span className="dot"></span>
                <li>
                  <a href={el.link} target="_blank">{el.title}</a>
                </li>
              </span>
          })}
        </ul>
      </div>
      <div className="close">
        <span onClick={handleShow}>Close
          <i className="fas fa-times-circle"></i>
        </span>
      </div>
    </div>
  )
}

export { Article, Bookmark, ConOpenNav, ConContainer };