import React from 'react';
import { Link } from 'react-router-dom';

const LogoBig = () => {
  return (
    <div className="login__logo-big">
      <Link to="/">
        <svg>
          <circle cx="10%" cy="50%" r="10%" fill="#00ffff" />
          <circle cx="29%" cy="50%" r="10%" fill="#ff00ff" />
          <circle cx="48%" cy="50%" r="10%" fill="#ffff00" />
          <defs>
            <linearGradient id="grad1" x1="0%" y1="0%" x2="0%" y2="100%">
              <stop offset="0%" style={{stopColor:"rgb(143,148,158)",stopOpacity:1}} />
              <stop offset="100%" style={{stopColor:"rgb(201,204,211)",stopOpacity:1}} />
            </linearGradient>
          </defs>
          <circle cx="67%" cy="50%" r="10%" fill="url(#grad1)" />
          <text x="63%" y="65%" id="k" className="login-brand-name">K</text>
          <text x="74%" y="65%" className="login-brand-name">nittr</text>
        </svg>
      </Link>
    </div>
  )
}

const LogoSmall = () => {
  return (
    <div className="login__logo-small">
      <Link to="/">
        <svg>
          <circle cx="5%" cy="50%" r="7%" fill="#00ffff" />
          <circle cx="18%" cy="50%" r="7%" fill="#ff00ff" />
          <circle cx="31%" cy="50%" r="7%" fill="#ffff00" />
          <defs>
            <linearGradient id="grad1" x1="0%" y1="0%" x2="0%" y2="100%">
              <stop offset="0%" style={{stopColor:"rgb(143,148,158)",stopOpacity:1}} />
              <stop offset="100%" style={{stopColor:"rgb(201,204,211)",stopOpacity:1}} />
            </linearGradient>
          </defs>
          <circle cx="44%" cy="50%" r="7%" fill="url(#grad1)" />
          <text x="41%" y="75%" id="k" className="login-brand-name">K</text>
          <text x="49%" y="75%" className="login-brand-name">nittr</text>
        </svg>
      </Link>
    </div>
  )
}

export { LogoBig, LogoSmall };