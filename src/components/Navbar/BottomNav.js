import React, { Component } from 'react';

class Bottomnav extends Component {
  constructor(props) {
    super(props);
    this.state = {
      width: window.innerWidth
    }
  }

  componentDidMount = () => {
    window.addEventListener("resize", this.updateWidth);
  }

  updateWidth = () => {
    this.setState({
      width: window.innerWidth
    });
  }

  componentWillUnmount = () => {
    window.removeEventListener("resize", this.updateWidth);
  }
  
  render() {
    let {width} = this.state;
    let temp = width < 600;
    return (
      <div className="bottomnav">
        <p><i className="fas fa-th-large"></i>{temp ? null : "Feeds"}</p>
        <p><i className="fas fa-search"></i>{temp ? null : "Search"}</p>
        <p><i className="fas fa-cog"></i>{temp ? null : "Settings"}</p>
        <p><i className="fas fa-user"></i>{temp ? null : "Profile"}</p>
      </div>
    );
  }
}

export default Bottomnav;