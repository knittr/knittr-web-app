import React from "react";
import WordCloud from 'react-d3-cloud';


const WordsCloud = (props) => {
  let {data} = props;
  const fontSizeMapper = word => Math.log2(word.value) * 20;
  const rotate = () => Math.floor(Math.random() * 4)*90;

  return(
    <WordCloud
      width={window.innerWidth * .5}
      height={window.innerHeight * .5}
      data={data}
      fontSizeMapper={fontSizeMapper}
      rotate={rotate}
    />
  )
}

export default WordsCloud;