import React, { Component } from 'react';
import Skeleton from 'react-loading-skeleton';
import ScrollMenu from 'react-horizontal-scrolling-menu';
import uuidv4 from 'uuid/v4';


const Menu = (list) => list.map(el => <div className="menu-item" key={uuidv4()}>{el}</div>);
 
const Arrow = ({ text, className }) => {
  return (
    <div
      className={className}
    >{text}</div>
  );
};
 
 
const ArrowLeft = Arrow({ text: '<', className: 'arrow-left' });
const ArrowRight = Arrow({ text: '>', className: 'arrow-right' });

class ArticleThumbnail extends Component {
  state = {
    selected: 0
  };
  
  onSelect = key => {
    this.setState({ selected: key });
  }

  render() {
    let { thumbnail, headline, tags } = this.props;
    let tagsCopy = Array.from(tags, x => x);
    let set1 = Menu(tagsCopy.splice(0, Math.floor(tagsCopy.length / 2)));
    let set2 = Menu(tagsCopy);
    return (
      <div className="articlethumbnail"
        style={{
          backgroundImage: `url(${thumbnail})`
        }}>
        <div
          style={{
            backgroundImage: `linear-gradient(to bottom, rgb(0,0,0), rgba(0,0,0, .45), rgba(0,0,0,0), rgba(0,0,0,0), rgba(0,0,0,.44), rgb(0,0,0))`,
          }}>
          <div className="articlethumbnail__container">
            <div className="articlethumbnail__container-details">
              <div className="headline">
                <h2>{headline ? headline : <Skeleton count={2}/>}</h2>
                <span onClick={() => this.props.history.goBack()} className="close">X</span>
              </div>
              <span className="rating">{headline ? 9 : <Skeleton />}</span>
              <span className="subject">{headline ? "India" : null}</span>
            </div>
            { headline ? <div className="articlethumbnail__tags">
              <div className="list-1">
                <ScrollMenu 
                  alignCenter={false}
                  data={set1}
                  arrowLeft={ArrowLeft}
                  arrowRight={ArrowRight}
                  selected={this.state.selected}
                  onSelect={this.onSelect}
                />
              </div>
              <div className="list-2">
                <ScrollMenu 
                  alignCenter={false}
                  data={set2}
                  arrowLeft={ArrowLeft}
                  arrowRight={ArrowRight}
                  selected={this.state.selected}
                  onSelect={this.onSelect}
                />
              </div>
            </div> : null}
          </div>
        </div>
      </div>
    );
  }
}

export default ArticleThumbnail;