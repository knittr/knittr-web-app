import React from 'react';
import Skeleton from 'react-loading-skeleton';
import {Link} from 'react-router-dom';

const Feed = (props) => {
  let { id, thumbnail, headline, overview } = props.item;
  return (
    <Link to={id ? `/app/article/${id}` : '/'}>
      <div
      className="feed"
        style={{
          backgroundImage: `url(${thumbnail})`
        }}
      >
        <div 
          className="feed__container"
          style={{
            opacity: .85,
            backgroundImage: `linear-gradient(to bottom, rgb(0,0,0), rgba(0,0,0, .45), rgba(0,0,0,0), rgba(0,0,0,0), rgba(0,0,0,.44), rgb(0,0,0))`,
          }}
        >
          <div className="feed__container-subject">
            <span>{id ? "India" : <Skeleton width={50} />}</span>
            <span className="rating">{id ? 9 : <Skeleton width={20} />}</span>
          </div>
          <div className="feed__container-title">
          <h3>{headline ? headline.length > 90 ? headline.slice(0,60)+"..." : `${headline}.` : <Skeleton count={2}/>}</h3>
          </div>
          <div className="feed__container-overview">
            <h4>
              {overview && overview.length > 150 ? overview.slice(0,140)+"..." : overview || <Skeleton count={4} />}
            </h4>
          </div>
        </div>
      </div>
    </Link>
  );
}
export default Feed;