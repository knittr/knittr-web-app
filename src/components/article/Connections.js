import React, { Component } from 'react';
import posed from 'react-pose';
import { Scrollbars } from 'react-custom-scrollbars';

const Box = posed.div({
  enter: {
    opacity: 1
  },
  exit: { 
    opacity: 0
  }
});

class CustomScroll extends Component {
  renderView = ({style, ...props}) =>{
    return (
      <div
        style={{ ...style }}
        {...props}
      />
    );
  }

  renderThumb = ({ style, ...props}) => {
    return (
      <div
        style={{...style, backgroundColor: "#ffffffa8", borderRadius: 5}}
        {...props}
      />
    )
  }

  render() {
    return (
      <Scrollbars
        autoHide
        renderView={this.renderView}
        renderThumbVertical={this.renderThumb}
        {...this.props} />
    )
  }
}

const ConnectionCard = (props) => {
  return(
    <div className="card">
      <div className="card__date">
        <span className="date__container">
          <span className="date__container-date">
            2
          </span>
          <span className="date__container-monthyear">
            <span>Nov</span>
            <span>2018</span>
          </span>
        </span>
        <span className="day">Friday</span>
      </div>
      <div className="card__sources">
        {props.links.map((item, i) => <p key={i}>
          <span>{item.source}: </span>
            <a href={item.link} target="_blank">{item.title}
            </a>
          </p>
        )}
      </div>
    </div>
  )
}

class Connections extends Component {

  componentDidMount = () => {
    document.querySelector(".connections").addEventListener("click", (evt) => {
      const connectionContainer = document.querySelector(".connections__container");
      let targetElement = evt.target; // clicked element
      do {
          if (targetElement == connectionContainer) return;
          // Go up the DOM
          targetElement = targetElement.parentNode;
      } while (targetElement);
      // This is a click outside.
      if(this.props.viewConnections) this.props.toggleConnections();
    });
  }

  render() {
    return (
      <Box>
        <div className="connections">
          <div className="connections__container">
            <div className="connections__container-title">
              <span>Connections</span>
              <span onClick={this.props.toggleConnections} className="close">X</span>
            </div>
            <div className="connections__container-cards">
              <CustomScroll>
                <ConnectionCard links={this.props.links} />
              </CustomScroll>
            </div>
          </div>
        </div>
      </Box>
    );
  }
}

export default Connections;