import React from 'react';
import Skeleton from 'react-loading-skeleton';

const ContentCard = (props) => {
  return (
    <div className="contentcard">
      <div className="contentcard__content">
        <p>{props.content ? props.content : <Skeleton count={10} />}</p>
      </div>
    </div>
  );
}

export default ContentCard;