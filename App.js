import React, { Component } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { createStore, applyMiddleware, compose } from "redux";
import { Provider } from "react-redux";
import thunk from "redux-thunk";
import "flexboxgrid/dist/flexboxgrid.min.css";

import rootReducer from "./src/reducers/index";
import Feeds from "./src/containers/Feeds";
import Article from "./src/containers/Article";
import CustomAnalysis from "./src/containers/CustomAnalysis";
import NewCustom from "./src/containers/NewCustom";
import { PageNotFound } from "./src/components/util/errorComp";
import "./src/styles/_base.scss";

//TODO: remove devtools when building for production
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const enhancer = composeEnhancers(applyMiddleware(thunk));
const store = createStore(rootReducer, applyMiddleware(thunk));

//TODO: create a protected route to authenticate
class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Router>
          <Switch>
            <Route exact path="/" component={NewCustom} />
            <Route exact path="/app/article/:id" component={Article} />
            <Route component={PageNotFound} />
          </Switch>
        </Router>
      </Provider>
    );
  }
}

export default App;
